import { useMessagesContext } from "../utils/MessagesContext"
import styled from "styled-components"
import { StoreState } from "../utils/AppReducer"
import MessagesColumn from "./MessagesColumn"

const ColumnsLayout = styled.div`
  display: grid;
  grid-template-columns: repeat(3, 1fr);
  width: 100%;
  padding: 3em 10vw;
  gap: 1em;
`

const Layout = () => {
  const { store } = useMessagesContext()
  const { error, warning, info } = (store as StoreState)?.messages

  return (
    <ColumnsLayout>
      <MessagesColumn messagesList={error} priority="error" />
      <MessagesColumn messagesList={warning} priority="warning" />
      <MessagesColumn messagesList={info} priority="info" />
    </ColumnsLayout>
  )
}

export default Layout
