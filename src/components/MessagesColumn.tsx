import { Button, Paper, Snackbar } from '@material-ui/core'
import { useEffect, useState, useRef } from 'react'
import styled from 'styled-components'
import { Message } from '../Api'
import { Dispatches } from '../utils/AppReducer'
import { useMessagesContext } from '../utils/MessagesContext'
import MessageComponent from './Message'

const StyledPaper = styled(Paper)`
  padding: 1em 2em;
  cursor: pointer;
`

interface props {
  messagesList: Array<Message>,
  priority: string
}

const MessagesColumn = ({messagesList, priority} :props) => {
  const { dispatch } = useMessagesContext()
  const [showSnack, setShowSnack] = useState<boolean>(false)
  const interval = useRef(0)

  const clearColumn = () => dispatch?.({
    type: Dispatches.CLEAR_ALL_PRIORITY,
    payload: priority
  })

  useEffect(() => {
    if (priority === 'error' && messagesList.length > 0) {
      setShowSnack(true)
    }

    interval.current = setTimeout(() => {
      setShowSnack(false)
    }, 2000)

    return () => {
      setShowSnack(false)
      clearInterval(interval.current)
    }
  }, [messagesList.length, priority])

  const snackMessage = messagesList[messagesList.length -1]?.message

  return (
    <div>
      {Boolean(messagesList.length) && <Button onClick={clearColumn}>Clear this column</Button>}
      {messagesList.map((message, index) => (
        <MessageComponent
          key={message.message}
          message={message}
          index={index}
        />
      ))}

      <Snackbar
        anchorOrigin={{ vertical: 'top', horizontal: 'right' }}
        open={showSnack}
        key={snackMessage}
      >
        <StyledPaper
          data-testid="snackbarcontent"
          elevation={5}
          onClick={() => setShowSnack(false)}
          
        >
          {snackMessage}
        </StyledPaper>
      </Snackbar>
    </div>
  )
}

export default MessagesColumn
