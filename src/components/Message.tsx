import { Message } from '../Api'
import { Paper, Button, Typography } from '@material-ui/core'
import styled from 'styled-components'
import { Dispatches } from '../utils/AppReducer'
import { useMessagesContext } from '../utils/MessagesContext'

const CardBackgroundColors = {
  '0': '#F56236',
  '1': '#FCE788',
  '2': '#88FCA3'
}

interface MessageProps {
  message: Message
  index: number
}

const StyledPaper = styled(Paper)`
  padding: 2em 3em;
  width: auto;
  display: flex;
  justify-content: space-between;
  align-items: center;
  font-size: 1.3em;
  &:not(:last-child) {
    margin-bottom: 1em;
  }
`

const MessageComponent = ({index, message}: MessageProps) => {
  const cardColorStyle = {
    backgroundColor: CardBackgroundColors[message.priority]
  }

  const { dispatch } = useMessagesContext()

  const removeMessage = (index:number, priority: number) => dispatch?.({
    type: Dispatches.REMOVE_MESSAGE,
    payload: {
      index, priority
    }
  })

  return (
    <StyledPaper elevation={2} style={cardColorStyle}>
      <Typography>
        {message.message}
      </Typography>
      <Button onClick={() => removeMessage(index, message.priority)}>Remove</Button>
    </StyledPaper>
  )
}

export default MessageComponent
