import React from "react";
import { Message } from '../Api'

export interface Action {
  type: Dispatches,
  payload?: any
}

interface MessagesList {
  error: Array<Message>
  warning: Array<Message>
  info: Array<Message>
}

export interface StoreState {
  subscribed: boolean
  messages: MessagesList
}

export const initalState :StoreState = {
  subscribed: true,
  messages: {
    error: [],
    warning: [],
    info: []
  },
}

const priorityTypes = {
  0: 'error',
  1: 'warning',
  2: 'info'
}

export enum Dispatches {
  ADD_MESSAGE,
  REMOVE_MESSAGE,
  CLEAR_ALL_MESSAGES,
  SUBSCRIBE,
  UNSUBSCRIBE,
  CLEAR_ALL_PRIORITY
}

export const AppReducer: React.Reducer<StoreState, Action> = (state: StoreState, action: Action) :StoreState => {
  let priority
  switch (action.type) {
    case Dispatches.ADD_MESSAGE:
        priority = priorityTypes[(action.payload as Message).priority]
        return {
          ...state,
          messages: {
            ...state.messages,
            [priority]: [...state.messages[priority as keyof MessagesList], action.payload]
          }
        }
    case Dispatches.REMOVE_MESSAGE:
        priority = priorityTypes[(action.payload as Message).priority]
        return {
          ...state,
          messages: {
            ...state.messages,
            [priority]: state.messages[priority as keyof MessagesList].filter((_, index) => index !== action.payload.index)
          }
        }
    case Dispatches.CLEAR_ALL_MESSAGES:
        return {
          ...state,
          messages: {
            error: [],
            warning: [],
            info: []
          },
        }
    case Dispatches.SUBSCRIBE:
        return {
          ...state,
          subscribed: true
        }
    case Dispatches.UNSUBSCRIBE:
        return {
          ...state,
          subscribed: false
        }
    case Dispatches.CLEAR_ALL_PRIORITY:
      priority = action.payload
      return {
        ...state,
        messages: {
          ...state.messages,
          [priority]: []
        }
      }
    default:
      return state
  }
}