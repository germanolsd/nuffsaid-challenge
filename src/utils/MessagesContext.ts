import { useContext, createContext, Dispatch } from 'react'
import { Action, StoreState } from './AppReducer'

interface ContextProvider {
  store?: StoreState | object,
  dispatch?: Dispatch<Action>
}

const initialState: ContextProvider = {
  store: {},
  dispatch: () => {}
}

export const MessagesContext = createContext(initialState)

export const useMessagesContext = () => useContext<ContextProvider>(MessagesContext)
