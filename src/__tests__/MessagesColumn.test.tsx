import React from 'react';
import { render, waitFor } from '@testing-library/react';
import MessagesColumn from '../components/MessagesColumn';

const props = {
  messagesList: [
    {
      message: 'some message text',
      priority: 0
    }
  ],
  priority: 'error'
}
const propsTypeWarning = {
  messagesList: [
    {
      message: 'some message text',
      priority: 1
    }
  ],
  priority: 'warning'
}
const propsWithoutMessages = {
  messagesList: [],
  priority: 'error'
}

test('renders the component', () => {
  const comp = render(<MessagesColumn {...props} />);
  expect(comp).toBeTruthy();
});

test('Clear button is there if there are messages', async () => {
  const {getByText} = render(<MessagesColumn {...props} />);
  await waitFor(() => {
    expect(getByText('Clear this column')).toBeInTheDocument()
  })
})

test('Clear button is not there if there aren\'t any messages', () => {
  const {queryByText} = render(<MessagesColumn {...propsWithoutMessages} />);
  const el = queryByText('Clear this column')

  expect(el).not.toBeInTheDocument()
})

test('Snackbar shows itself if message is of priority type error', async () => {
  const {getByTestId} = render(<MessagesColumn {...props} />);
  await waitFor(() => {
    expect(getByTestId('snackbarcontent')).toBeInTheDocument()
  })
})

test('Snackbar doesn\'t show itself if message is not of priority type error', () => {
  const {queryByTestId, getByText} = render(<MessagesColumn {...propsTypeWarning} />);
  expect(queryByTestId('snackbarcontent')).not.toBeInTheDocument()
  expect(getByText('some message text')).toBeInTheDocument()
})


