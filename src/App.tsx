import React, { useReducer, useCallback, useState } from 'react';
import { useEffect } from 'react';
import generateMessage, { Message } from './Api';
import { MessagesContext } from './utils/MessagesContext' 
import { AppReducer, initalState, Dispatches } from './utils/AppReducer'
import Layout from './components/Layout';
import './app.css'
import { Button, FormControlLabel, FormGroup, Switch } from '@material-ui/core';

const App: React.FC<{}> = () => {
  const [store, dispatch] = useReducer(AppReducer, initalState)

  const addMessage = (payload: Message) => {
    dispatch({
      type: Dispatches.ADD_MESSAGE,
      payload
    })
  }

  const unsubscribe = useCallback(
    () => {
      dispatch({
        type: Dispatches.UNSUBSCRIBE
      })
    },
    [dispatch],
  )

  const clearAll = useCallback(
    () => {
      dispatch({
        type: Dispatches.CLEAR_ALL_MESSAGES
      })
    },
    [dispatch],
  )

  const setSubscription = () => {
    if (store.subscribed) {
      unsubscribe()
    } else {
      subscribe()
    }
  }

  const subscribe = useCallback(
    () => {
      dispatch({
        type: Dispatches.SUBSCRIBE
      })
    },
    [dispatch],
  )

  useEffect(() => {
    let unsubscribe: null | (() => void) = null
    if (store.subscribed) {
      unsubscribe = generateMessage((message: Message) => {
        addMessage(message);
      });
    }

    return () => unsubscribe?.()
  }, [store.subscribed]);

  return (
    <MessagesContext.Provider value={{
      store,
      dispatch
    }}>
      <FormGroup>
        <FormControlLabel label="Receive new messages" control={
          <Switch 
            onChange={setSubscription}
            checked={store.subscribed}
          />
        }/>
      </FormGroup>

      <Button style={{color: 'red'}} onClick={clearAll}>Clear all messages</Button>
      <Layout />
    </MessagesContext.Provider>
  );
}

export default App;
